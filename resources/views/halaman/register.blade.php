<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>

<body>
    <h1>Buat Account Baru</h1>
    <h4>Sign Up Form</h4>

    <form action="/selamat_datang" method="post">
        @csrf

        <label for="first_name">First Name:</label><br><br>
        <input type="text" name="first_name"><br><br>

        <label for="last_name">Last Name:</label><br><br>
        <input type="text" name="last_name">

        <p>Gender:</p>
        <input type="radio" name="male" id="male" value="Male">
        <label for="male">Male</label><br>

        <input type="radio" name="female" id="female" value="female">
        <label for="Female">Female</label><br>

        <input type="radio" name="other" id="other" value="Other">
        <label for="Other">Other</label><br>


        <p>Nationality:</p>
        <select name="nationality" id="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Germany">Germany</option>
            <option value="England">England</option>
        </select>

        <p>Language</p>
        <input type="checkbox" id="language1" name="language1" value="Indonesia">
        <label for="language1"> Indonesia</label><br>

        <input type="checkbox" id="language2" name="language2" value="English">
        <label for="language2">English</label><br>

        <input type="checkbox" id="language3" name="language3" value="Other">
        <label for="language3">Other</label><br>

        <p>Bio</p>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>

        <input type="submit" value="Sign Up">

    </form>
</body>

</html>